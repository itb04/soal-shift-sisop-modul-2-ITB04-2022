#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
//#include <json-c/json.h>

void download(const char *unique_id) {
    char link[100] = "";
    char path[100];
    sprintf(path,"/home/%s/soal-shift-sisop-modul-2-ITB04-2022/soal1/", getlogin());
    sprintf(link, "https://drive.google.com/uc?id=%s", unique_id);
    if(fork() == 0) {
        char *argv[] = {"wget", "-q", link, path, NULL};
        execv("/bin/wget", argv);
    }
    wait(NULL);

    char unzip[100] = "";
    sprintf(unzip, "uc?id=%s", unique_id);
    if(fork() == 0) {
        sleep(8);
        char *argv[] = {"unzip", "-q", unzip, NULL};
        execv("/bin/unzip", argv);
    }
    wait(NULL);

    if(fork() == 0) {
        char *argv[] = {"rm", "-rf", unzip, NULL};
        execv("/bin/rm", argv);
    }
    wait(NULL);
}

int main() {
    download("1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp");
    download("1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT");

    char path_gacha[100];
    sprintf(path_gacha, "/home/%s/soal-shift-sisop-modul-2-ITB04-2022/soal1/gacha_gacha", getlogin());
    if(fork() == 0) {
        char *argv[] = {"mkdir", "-p", path_gacha, NULL};
        execv("/bin/mkdir", argv);
    }
    wait(NULL);
}