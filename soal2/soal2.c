#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

typedef struct
{
    char judul[256];
    int tahun;
    char genre[64];
    char oldfilename[256];
} Drakor;

int compare (const void* a, const void* b)
{
    Drakor *dataA = (Drakor *)a;
    Drakor *dataB = (Drakor *)b;

    return(dataA->tahun - dataB->tahun);
}

void makeDirectory(char *zip_path)
{
    if (fork() == 0) {
        char *argv[] = {"mkdir", "-p", zip_path, NULL};
        execv("/bin/mkdir", argv);
    }
    wait(NULL);
}

void unzipFiles(char *zip_path) {
    if(fork() == 0) {
        char *argv[] = {"unzip", "-qo", "./drakor.zip", "-d", zip_path, NULL};
        execv("/bin/unzip", argv);
    }
    wait(NULL);
}

void deleteFiles(char *zip_path) {
    if(fork() == 0)
    {
        chdir(zip_path);
        char *argv[] = {"sh", "-c", "find -mindepth 1 -maxdepth 1 ! -iname '*.png' -exec rm -r -- {} +", NULL};
        execv("/bin/sh", argv);
    }
    wait(NULL);
}

void filterFiles(char *basePath, Drakor *drakor)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    int i = 0;

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        char removepath[512] = "";
        char destinationpath[512] = "";
        char oldfilename[512] = "";
        char newfilename[512] = "";
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            if(!(strstr(dp->d_name, "_")))
            {
                strcpy(drakor[i].oldfilename, dp->d_name);
                
                char *tmp;
                tmp = strtok(dp->d_name, ";");
                strcpy(drakor[i].judul, tmp);
                
                tmp = strtok(NULL, ";");
                drakor[i].tahun = atoi(tmp);
                
                tmp = strtok(NULL, ";.");
                strcpy(drakor[i].genre, tmp);

                sprintf(destinationpath, "%s%s", basePath, drakor[i].genre);
                
                if(fork()==0)
                {
                    char *argv[] = {"mkdir", "-p", destinationpath, NULL};
                    execv("/bin/mkdir", argv);
                }
                wait(NULL);

                strcat(oldfilename, basePath);
                strcat(oldfilename, drakor[i].oldfilename);

                strcat(newfilename, destinationpath);
                strcat(newfilename, "/");
                strcat(newfilename, drakor[i].judul);
                strcat(newfilename, ".png");

                
                if(fork()==0)
                {
                    char *argv[] = {"mv", "-f", oldfilename, newfilename};
                    execv("/bin/mv", argv);
                }
                wait(NULL);
            }
            else
            {
                strcpy(drakor[i].oldfilename, dp->d_name);
                strcpy(drakor[i+1].oldfilename, dp->d_name);
                
                char *tmp;
                
                tmp = strtok(dp->d_name, ";");
                strcpy(drakor[i].judul, tmp);
                tmp = strtok(NULL, ";");
                drakor[i].tahun = atoi(tmp);
                tmp = strtok(NULL, "_");
                strcpy(drakor[i].genre, tmp);
            
                sprintf(destinationpath, "%s%s", basePath, drakor[i].genre);

                if(fork()==0)
                {
                    char *argv[] = {"mkdir", "-p", destinationpath, NULL};
                    execv("/bin/mkdir", argv);
                }
                wait(NULL);
                
                strcat(oldfilename, basePath);
                strcat(oldfilename, drakor[i].oldfilename);

                strcat(newfilename, destinationpath);
                strcat(newfilename, "/");
                strcat(newfilename, drakor[i].judul);
                strcat(newfilename, ".png");

                if(fork()==0)
                {
                    char *argv[] = {"cp", "-f", oldfilename, newfilename};
                    execv("/bin/cp", argv);
                }
                wait(NULL);

                tmp = strtok(NULL, ";");
                strcpy(drakor[i+1].judul, tmp);

                tmp = strtok(NULL, ";");
                drakor[i+1].tahun = atoi(tmp);

                tmp = strtok(NULL, ";.");
                strcpy(drakor[i+1].genre, tmp);

                strcpy(destinationpath, "");
                strcpy(oldfilename, "");
                strcpy(newfilename, "");
            
                sprintf(destinationpath, "%s/%s", basePath, drakor[i+1].genre);
                
                if(fork()==0)
                {
                    char *argv[] = {"mkdir", "-p", destinationpath, NULL};
                    execv("/bin/mkdir", argv);
                }
                wait(NULL);
                
                strcat(oldfilename, basePath);
                strcat(oldfilename, drakor[i+1].oldfilename);

                strcat(newfilename, destinationpath);
                strcat(newfilename, "/");
                strcat(newfilename, drakor[i+1].judul);
                strcat(newfilename, ".png");

                if(fork()==0)
                {
                    char *argv[] = {"mv", "-f", oldfilename, newfilename};
                    execv("/bin/mv", argv);
                }
                wait(NULL);
                i++;
            }
            i++;
        }
    }
    closedir(dir);
}


void createData(char *basePath, Drakor *drakor)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    FILE *file;

    char kategori[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        char datapath[100] = "";
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && dp->d_type == 4)
        {
            strcat(datapath, basePath);
            strcat(datapath, dp->d_name);
            strcat(datapath, "/data.txt");
            
            int x;
            int y = 0;
            Drakor temp[10];
            for(x = 0; x<20; x++)
            {            
                if(strcmp(drakor[x].genre, dp->d_name) == 0)
                {
                    strcpy(temp[y].judul, drakor[x].judul);
                    temp[y].tahun = drakor[x].tahun;
                    strcpy(temp[y].genre, drakor[x].genre);
                    y++;
                }
            }
            qsort(temp, y, sizeof(Drakor), compare);
            file = fopen(datapath, "a");
            fprintf(file, "kategori: %s\n\n", dp->d_name);
            for(int f = 0; f<y; f++)
            {
                fprintf(file, "nama : %s\n", temp[f].judul);
                fprintf(file, "rilis : tahun %d\n\n", temp[f].tahun);
            }
        }
    }
    closedir(dir);
}

int main() {
    char zip_path[100];
    sprintf(zip_path, "/home/%s/shift2/drakor/", getlogin());

    Drakor drakor[20];

    makeDirectory(zip_path);
    unzipFiles(zip_path);
    deleteFiles(zip_path);
    filterFiles(zip_path, drakor);
    createData(zip_path, drakor);

    return 0;
}