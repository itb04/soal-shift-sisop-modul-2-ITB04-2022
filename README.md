# soal-shift-sisop-modul-1-ITB04-2022
## Laporan Pengerjaan Soal Shift Modul 1 Praktikum Sistem Operasi

Nama Anggota Kelompok:
 1. Hafizh Abid Wibowo - 5027201011
 2. Axellino Anggoro Armandito - 5027201040
 3. Anak Agung Bintang - 5027201060



# Daftar Isi
- [soal-shift-sisop-modul-1-ITB04-2022](#soal-shift-sisop-modul-1-itb04-2022)
  - [Laporan Pengerjaan Soal Shift Modul 1 Praktikum Sistem Operasi](#laporan-pengerjaan-soal-shift-modul-1-praktikum-sistem-operasi)
- [Daftar Isi](#daftar-isi)
- [Soal 1](#soal-1)
    - [Soal 1a](#soal-1a)
      - [Output 1a](#output-1a)
- [Soal 2](#soal-2)
    - [Soal 2a](#soal-2a)
      - [Output 2a](#output-2a)
    - [Soal 2b 2c 2d](#soal-2b-2c-2d)
      - [Output 2b](#output-2b)
      - [Output 2c dan 2d](#output-2c-dan-2d)
    - [Soal 2e](#soal-2e)
      - [Output 2e](#output-2e)
- [Soal 3](#soal-3)
    - [Soal 3a](#soal-3a)
      - [Output 3a](#output-3a)
    - [Soal 3b](#soal-3b)
    - [Soal 3c](#soal-3c)
    - [Soal 3d](#soal-3d)
      - [Output 3d](#output-3d)
    - [Soal 3e](#soal-3e)
      - [Output 3e](#output-3e)
- [Kendala Pengerjaan](#kendala-pengerjaan)
    - [Soal 1](#soal-1-1)
    - [Soal 3](#soal-3-1)

---

# Soal 1
### Soal 1a
Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama `gacha_gacha`

<br>

Pada snippet ini digunakan untuk menginisialisasi variabel yang berhubungan dengan link download database dan path direktori untuk menyimpan file

``` c
void download(const char *unique_id) {
    char link[100] = "";
    char path[100];
    sprintf(path,"/home/%s/soal-shift-sisop-modul-2-ITB04-2022/soal1/", getlogin());
    sprintf(link, "https://drive.google.com/uc?id=%s", unique_id);
    ...
```

- `sprintf(path,"/home/%s/soal-shift-sisop-modul-2-ITB04-2022/soal1/", getlogin())` fungsi `getlogin()` digunakan untuk memperoleh *user id* praktikan. Kemudian pada baris ini akan menunjukkan tempat menyimpan file database yang disimpan pada variabel *path*
- `sprintf(link, "https://drive.google.com/uc?id=%s", unique_id)` digunakan untuk mendapatkan link download file database yang disimpan pada variabel *link*
  
Selanjutnya untuk mendownload dan mengekstrak file akan dijelaskan pada fungsi berikut:

``` c
if(fork() == 0) {
        char *argv[] = {"wget", "-q", link, path, NULL};
        execv("/bin/wget", argv);
    }
    wait(NULL);

    char unzip[100] = "";
    sprintf(unzip, "uc?id=%s", unique_id);
    if(fork() == 0) {
        sleep(8);
        char *argv[] = {"unzip", "-q", unzip, NULL};
        execv("/bin/unzip", argv);
    }
    wait(NULL);

    ...
    sprintf(path_gacha, "/home/%s/soal-shift-sisop-modul-2-ITB04-2022/soal1/gacha_gacha", getlogin());
    if(fork() == 0) {
        char *argv[] = {"mkdir", "-p", path_gacha, NULL};
        execv("/bin/mkdir", argv);
    }
```

- `if(fork() == 0)` Untuk menandakan program berjalan pada child proses
- `wget -q` digunakan untuk menjalankan command download file dengan tidak menampilkan output pada terminal
- `sprintf(unzip, "uc?id=%s", unique_id)` untuk memanggil unique id pada link download dan disimpan pada variabel *unzip*
- `sleep(8)` untuk melakukan delay pada program sebanyak 8 detik
- `unzip -q` digunakan untuk menjalankan command extract file dengan tidak menampilkan output pada terminal
- `execv("/bin/wget", argv) dan execv("/bin/unzip", argv)` berfungsi untuk menjalankan perintah  wget ataupun unzip dengan menggantikan proses child yang sedang berjalan
- `sprintf(path_gacha, "/home/%s/soal-shift-sisop-modul-2-ITB04-2022/soal1/gacha_gacha", getlogin())` digunakan untuk mendapatkan *user id* praktikan dan membuat folder `gacha_gacha` yang disimpan pada variabel *path_gacha*
- `mkdir -p` digunakan untuk membuat direktori baru
- `wait(NULL)` digunakan untuk menunggu child process satu selesai sebelum berpindah ke proses selanjutnya 

#### Output 1a
![output_1a](img/output_1a.png)

# Soal 2
### Soal 2a
Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder `/home/[user]/shift2/drakor`. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

<br>

Pertama buat fungsi untuk membuat direktori sesuai format nama yang telah ditentukan sebagai berikut:
```c
void makeDirectory(char *zip_path)
{
    if (fork() == 0) {
        char *argv[] = {"mkdir", "-p", zip_path, NULL};
        execv("/bin/mkdir", argv);
    }
    wait(NULL);
}
```

Kemudian untuk melakukan extract dari file tadi digunakan fungsi *unzipFiles* sebagai berikut:
```c
void unzipFiles(char *zip_path) {
    if(fork() == 0) {
        char *argv[] = {"unzip", "-qo", "./drakor.zip", "-d", zip_path, NULL};
        execv("/bin/unzip", argv);
    }
    wait(NULL);
}
```

Selanjutnya untuk membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan menggunakan fungsi *deleteFiles*
```c
void deleteFiles(char *zip_path) {
    if(fork() == 0)
    {
        chdir(zip_path);
        char *argv[] = {"sh", "-c", "find -mindepth 1 -maxdepth 1 ! -iname '*.png' -exec rm -r -- {} +", NULL};
        execv("/bin/sh", argv);
    }
    wait(NULL);
}
```
Pada snippet ini akan mencari file dengan format file gambar selain **.png** di dalam hasil unzip dari folder drakor menggunakan perintah `sh", "-c", "find -mindepth 1 -maxdepth 1 ! -iname '*.png'`. Selanjutnya hasil pencarian tadi akan dihapus menggunakan `-exec rm -r -- {} +", NULL`.

#### Output 2a
![output_2a](/img/output_2a.png)

### Soal 2b 2c 2d
2b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.
2c. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.
2d. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)

<br>

```c
void filterFiles(char *basePath, Drakor *drakor)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    int i = 0;

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        char removepath[512] = "";
        char destinationpath[512] = "";
        char oldfilename[512] = "";
        char newfilename[512] = "";
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            if(!(strstr(dp->d_name, "_")))
            {
                strcpy(drakor[i].oldfilename, dp->d_name);
                
                char *tmp;
                tmp = strtok(dp->d_name, ";");
                strcpy(drakor[i].judul, tmp);
                
                tmp = strtok(NULL, ";");
                drakor[i].tahun = atoi(tmp);
                
                tmp = strtok(NULL, ";.");
                strcpy(drakor[i].genre, tmp);

                sprintf(destinationpath, "%s%s", basePath, drakor[i].genre);
                
                if(fork()==0)
                {
                    char *argv[] = {"mkdir", "-p", destinationpath, NULL};
                    execv("/bin/mkdir", argv);
                }
                wait(NULL);

                strcat(oldfilename, basePath);
                strcat(oldfilename, drakor[i].oldfilename);

                strcat(newfilename, destinationpath);
                strcat(newfilename, "/");
                strcat(newfilename, drakor[i].judul);
                strcat(newfilename, ".png");

                
                if(fork()==0)
                {
                    char *argv[] = {"mv", "-f", oldfilename, newfilename};
                    execv("/bin/mv", argv);
                }
                wait(NULL);
            }
            else
            {
                strcpy(drakor[i].oldfilename, dp->d_name);
                strcpy(drakor[i+1].oldfilename, dp->d_name);
                
                char *tmp;
                
                tmp = strtok(dp->d_name, ";");
                strcpy(drakor[i].judul, tmp);
                tmp = strtok(NULL, ";");
                drakor[i].tahun = atoi(tmp);
                tmp = strtok(NULL, "_");
                strcpy(drakor[i].genre, tmp);
            
                sprintf(destinationpath, "%s%s", basePath, drakor[i].genre);

                if(fork()==0)
                {
                    char *argv[] = {"mkdir", "-p", destinationpath, NULL};
                    execv("/bin/mkdir", argv);
                }
                wait(NULL);
                
                strcat(oldfilename, basePath);
                strcat(oldfilename, drakor[i].oldfilename);

                strcat(newfilename, destinationpath);
                strcat(newfilename, "/");
                strcat(newfilename, drakor[i].judul);
                strcat(newfilename, ".png");

                if(fork()==0)
                {
                    char *argv[] = {"cp", "-f", oldfilename, newfilename};
                    execv("/bin/cp", argv);
                }
                wait(NULL);

                tmp = strtok(NULL, ";");
                strcpy(drakor[i+1].judul, tmp);

                tmp = strtok(NULL, ";");
                drakor[i+1].tahun = atoi(tmp);

                tmp = strtok(NULL, ";.");
                strcpy(drakor[i+1].genre, tmp);

                strcpy(destinationpath, "");
                strcpy(oldfilename, "");
                strcpy(newfilename, "");
            
                sprintf(destinationpath, "%s/%s", basePath, drakor[i+1].genre);
                
                if(fork()==0)
                {
                    char *argv[] = {"mkdir", "-p", destinationpath, NULL};
                    execv("/bin/mkdir", argv);
                }
                wait(NULL);
                
                strcat(oldfilename, basePath);
                strcat(oldfilename, drakor[i+1].oldfilename);

                strcat(newfilename, destinationpath);
                strcat(newfilename, "/");
                strcat(newfilename, drakor[i+1].judul);
                strcat(newfilename, ".png");

                if(fork()==0)
                {
                    char *argv[] = {"mv", "-f", oldfilename, newfilename};
                    execv("/bin/mv", argv);
                }
                wait(NULL);
                i++;
            }
            i++;
        }
    }
    closedir(dir);
}
```
Pada fungsi `filterFiles()` akan dilakukan looping directory pada folder hasil unzip **drakor.zip**. Dengan menggunakan struct Drakor akan menyimpan data-data seperti judul, tahun, genre, dan nama files sehingga akan memudahkan proses pembuatan **data.txt** tiap folder yang akan diurutkan secara ascending. Kemudian dengan menggunakan fungsi `strtok()` akan bisa memisah data-data yang ada pada namafiles. Karena terdapat dua nama files yang berbeda maka proses dipisah tergantung dengan tipe nama files. Kemudian file akan dipindah ke folder yang sesuai dengan genre yang ada pada file.

#### Output 2b
![output_2b](img/output_2b.png)

#### Output 2c dan 2d
> Kategori Action

![2cd_action](/img/2c_action.png)

>Kategori Comedy

![2cd_comedy](img/2c_comedy.png)

>Kategori Fantasy

![2cd_fantasy](img/2c_fantasy.png)

>Kategori Horror

![2cd_horror](img/2c_horror.png)

>Kategori Romance

![2cd_romance](img/2c_romance.png)

>Kategori School

![2cd_school](img/2c_school.png)

>Kategori Thriller

![2cd_thriller](img/2c_thriller.png)



### Soal 2e
Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.

<br>

```c
void createData(char *basePath, Drakor *drakor)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    FILE *file;

    char kategori[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        char datapath[100] = "";
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && dp->d_type == 4)
        {
            strcat(datapath, basePath);
            strcat(datapath, dp->d_name);
            strcat(datapath, "/data.txt");
            
            int x;
            int y = 0;
            Drakor temp[10];
            for(x = 0; x<20; x++)
            {            
                if(strcmp(drakor[x].genre, dp->d_name) == 0)
                {
                    strcpy(temp[y].judul, drakor[x].judul);
                    temp[y].tahun = drakor[x].tahun;
                    strcpy(temp[y].genre, drakor[x].genre);
                    y++;
                }
            }
            qsort(temp, y, sizeof(Drakor), compare);
            file = fopen(datapath, "a");
            fprintf(file, "kategori: %s\n\n", dp->d_name);
            for(int f = 0; f<y; f++)
            {
                fprintf(file, "nama : %s\n", temp[f].judul);
                fprintf(file, "rilis : tahun %d\n\n", temp[f].tahun);
            }
        }
    }
    closedir(dir);
}
```
Pada fungsi `createData()` maka akan dibuat files **data.txt** pada tiap folder genre yang ada. Dengan menggunakan looping directory listing maka akan dibuat temporary struct yang akan di sorting dengan fungsi `compare()`.

Berikut adalah code dari fungsi compare():

```c
int compare (const void* a, const void* b)
{
    Drakor *dataA = (Drakor *)a;
    Drakor *dataB = (Drakor *)b;

    return(dataA->tahun - dataB->tahun);
}
```
Kemudian setelah struct urut berdasarkan tahun maka akan memasukkan data sesuai dengan template yang ada pada soal setiap folder genrenya.

#### Output 2e
> Kategori Action

![2e_action](/img/2e_action.png)

>Kategori Comedy

![2e_comedy](img/2e_comedy.png)

>Kategori Fantasy

![2e_fantasy](img/2e_fantasy.png)

>Kategori Horror

![2e_horror](img/2e_horror.png)

>Kategori Romance

![2e_romance](img/2e_romance.png)

>Kategori School

![2e_school](img/2e_school.png)

>Kategori Thriller

![2e_thriller](img/2e_thriller.png)



# Soal 3
### Soal 3a
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

<br>

Pada source code di bawah ini akan memerintahkan program untuk membuat directory darat dan air

```c
    if(fork() == 0) {
        char darat[100];
        sprintf(darat, "/home/%s/modul2/darat", getlogin());
        char *argv[] = {"mkdir", "-p", darat, NULL};
        execv("/bin/mkdir", argv);
    }
    wait(NULL);
```
Code di atas akan dimulai dengan membuat child proses baru untuk membuat direktori **darat**. Variabel *darat* digunakan untuk menyimpan path sesuai format soal dan `getlogin*()` disini akan mendapatkan username linux praktikan. Baris selanjutnya akan menyimpan command untuk membuat direktori darat dengan `mkdir -p` kemudian command tersebut akan di run dengan `execv("/bin/mkdir", argv)`. Setelah itu `wait(NULL)` akan menunggu child proses selesai dipanggil.

```c
if(fork() == 0){
        sleep(3);
        char air[100];
        sprintf(air, "/home/%s/modul2/air", getlogin());
        char *argv[] = {"mkdir", "-p", air, NULL};
        execv("/bin/mkdir", argv);
    }
    wait(NULL);
```
Code di atas akan dimulai dengan membuat child proses baru untuk membuat direktori **air**. Sebelum masuk ke perintah selanjutnya proses akan mengalami delay selama 3 detik karena adanya `sleep(3)`. Variabel *air* digunakan untuk menyimpan path sesuai format soal dan `getlogin*()` disini akan mendapatkan username linux praktikan. Baris selanjutnya akan menyimpan command untuk membuat direktori darat dengan `mkdir -p` kemudian command tersebut akan di run dengan `execv("/bin/mkdir", argv)`. Setelah itu `wait(NULL)` akan menunggu child proses selesai dipanggil.

#### Output 3a
![3a](/img/3a.png)

### Soal 3b
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

<br>

```c
if(fork() == 0){
        char *argv[] = {"unzip", "-qo","./animal.zip", "-d", zippath, NULL};
        execv("/bin/unzip", argv);
    }
    wait(NULL);
```
Code di atas akan dimulai dengan membuat child proses baru untuk melakukan unzip pada file **animal.zip**. Baris selanjutnya akan menyimpan command untuk melakukan unzip dari animal.zip tanpa menampilkan output pada terminal dengan `unzip -qo`. Kemudian command tersebut akan di run dengan `execv("/bin/unzip", argv)`. Setelah itu `wait(NULL)` akan menunggu child proses selesai dipanggil.


### Soal 3c
Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

<br>

```c
if(fork() == 0){
        char *argv[] = {"sh", "-c", "find $HOME/modul2/animal/ | grep darat | xargs mv -t $HOME/modul2/darat"};
        execv("/bin/sh", argv);
    }
    wait(NULL);

if(fork() == 0){
        sleep(3);
        char *argv[] = {"sh", "-c", "find $HOME/modul2/animal/ | grep air | xargs mv -t $HOME/modul2/air"};
        execv("/bin/sh", argv);
    }
    wait(NULL);
```
Code di atas akan dimulai dengan membuat child proses baru untuk mencari keyword hewan darat dan air pada folder **animal** dengan menggunakan command `find $HOME/modul2/animal/ | grep darat` untuk hewan darat dan `find $HOME/modul2/animal/ | grep air` untuk hewan air. Setelah ditemukan file hewan darat dan air selanjutnya akan dipindahkan ke folder darat menggunakan `xargs mv -t $HOME/modul2/darat` dan ke folder air menggunakan `xargs mv -t $HOME/modul2/air`. Namun, untuk rentang pembuatan folder darat dan air akan didelay selama 3 detik dengan menggunakan `sleep(3)`. Terakhir `wait(NULL` akan menunggu child proses selesai dipanggil.

```c
if(fork() == 0){
        char *argv[] = {"rm", "-rf", removepath, NULL};
        execv("/bin/rm", argv);
    }
    wait(NULL);
```
Code ini akan dimulai dengan membuat child proses baru untuk menghapus hewan tanpa keyword air ataupun darat yang tersisa pada folder **animal** dengan menggunakan command `rm -rf`. Kemudian command tersebut akan di run menggunakan `execv("/bin/rm", argv)`. Terakhir, `wait(NULL)` akan menunggu child proses selesai dipanggil.

### Soal 3d
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

<br>

```c
if(fork() == 0){
        char *argv[] = {"sh", "-c", "find $HOME/modul2/darat/ | grep bird | xargs rm"};
        execv("/bin/sh", argv);
    }
    wait(NULL);
```
Snippet di atas akan dimulai dengan membuat child proses untuk menghapus semua file yang memiliki keyword **burung**. Pertama program akan dimulai dengan mencari list file di direktori darat yang mengandung keyword burung menggunakan `find $HOME/modul2/darat/ | grep bird`. Setelah ditemukan, maka file burung tersebut akan dihapus menggunakan `xargs rm`. Command akan di run menggunakan ` execv("/bin/sh", argv)` dan child proses akan ditunggu hingga selesai dipanggil dengan `wait(NULL)`

#### Output 3d
![3d_darat](/img/3d_darat.png)
![3d_air](/img/3d_air.png)

### Soal 3e
Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

<br>

```c
void makeList()
{
    char path[512];
    char pathlist[1000];

    sprintf(path, "/home/%s/modul2/air/", getlogin());
    sprintf(pathlist, "%slist.txt", path);

    struct dirent *dp;

    DIR *dir = opendir(path);
    FILE *list;

    while ((dp = readdir(dir)) != NULL)
    {
        struct stat fs;
        char filename[1000] = "";
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            sprintf(filename, "%s%s", path, dp->d_name);
            stat(filename, &fs);

            list = fopen(pathlist, "a");

            fprintf(list, "%s_", getlogin());
            if( fs.st_mode & S_IRUSR ) {
                fprintf(list, "r");
            } else {
                fprintf(list, "-");
            }

            if( fs.st_mode & S_IWUSR ) {
                fprintf(list, "w");
            } else {
                fprintf(list, "-");
            }

            if( fs.st_mode & S_IXUSR ) {
                fprintf(list, "x");
            } else {
                fprintf(list, "-");
            }
            fprintf(list, "_%s\n", dp->d_name);
        }
    }
    closedir(dir);
}
```

Untuk soal 3e kami menggunakan kombinasi antara template looping direktori listing dan template file permission yang ada di modul 2. Tiap looping nama file maka akan memasukkan list nama file sesuai format pada soal ke dalam **file.txt**

#### Output 3e
![3e_darat](/img/3d_darat.png)
![3e_air](/img/3d_air.png)

# Kendala Pengerjaan
### Soal 1
- Di soal ini kelompok kami kesulitan memahami logika program yang dibutuhkan dalam mengerjakan soal sehingga hanya dapat mengerjakan bagian 1a

### Soal 3
- Di soal ini kelompok kami menghadapi kendala saat mengerjakan soal 3e dimana program terus mengalami error dan tidak bisa membuat file list.txt. Tetapi kendala ini sudah berhasil diselesaikan dengan cara menambahkan fungsi `closedir()` 