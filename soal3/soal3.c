#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>

void makeList()
{
    char path[512];
    char pathlist[1000];

    sprintf(path, "/home/%s/modul2/air/", getlogin());
    sprintf(pathlist, "%slist.txt", path);

    struct dirent *dp;

    DIR *dir = opendir(path);
    FILE *list;

    while ((dp = readdir(dir)) != NULL)
    {
        struct stat fs;
        char filename[1000] = "";
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            sprintf(filename, "%s%s", path, dp->d_name);
            stat(filename, &fs);

            list = fopen(pathlist, "a");

            fprintf(list, "%s_", getlogin());
            if( fs.st_mode & S_IRUSR ) {
                fprintf(list, "r");
            } else {
                fprintf(list, "-");
            }

            if( fs.st_mode & S_IWUSR ) {
                fprintf(list, "w");
            } else {
                fprintf(list, "-");
            }

            if( fs.st_mode & S_IXUSR ) {
                fprintf(list, "x");
            } else {
                fprintf(list, "-");
            }
            fprintf(list, "_%s\n", dp->d_name);
        }
    }
    closedir(dir);
}

int main()
{
    char sourcepath[100];
    char zippath[100];
    char removepath[256];

    sprintf(sourcepath, "/home/%s/soal-shift-sisop-modul-2-ITB04-2022/soal3/animal.zip", getlogin());
    sprintf(zippath, "/home/%s/modul2", getlogin());
    sprintf(removepath, "%s/animal", zippath);

    if(fork() == 0) {
        char darat[100];
        sprintf(darat, "/home/%s/modul2/darat", getlogin());
        char *argv[] = {"mkdir", "-p", darat, NULL};
        execv("/bin/mkdir", argv);
    }
    wait(NULL);


    if(fork() == 0){
        sleep(3);
        char air[100];
        sprintf(air, "/home/%s/modul2/air", getlogin());
        char *argv[] = {"mkdir", "-p", air, NULL};
        execv("/bin/mkdir", argv);
    }
    wait(NULL);


    if(fork() == 0){
        char *argv[] = {"unzip", "-qo","./animal.zip", "-d", zippath, NULL};
        execv("/bin/unzip", argv);
    }
    wait(NULL);


    if(fork() == 0){
        char *argv[] = {"sh", "-c", "find $HOME/modul2/animal/ | grep darat | xargs mv -t $HOME/modul2/darat"};
        execv("/bin/sh", argv);
    }
    wait(NULL);
    
    if(fork() == 0){
        sleep(3);
        char *argv[] = {"sh", "-c", "find $HOME/modul2/animal/ | grep air | xargs mv -t $HOME/modul2/air"};
        execv("/bin/sh", argv);
    }
    wait(NULL);

    if(fork() == 0){
        char *argv[] = {"rm", "-rf", removepath, NULL};
        execv("/bin/rm", argv);
    }
    wait(NULL);

    if(fork() == 0){
        char *argv[] = {"sh", "-c", "find $HOME/modul2/darat/ | grep bird | xargs rm"};
        execv("/bin/sh", argv);
    }
    wait(NULL);
    
    makeList();
    return 0;
}